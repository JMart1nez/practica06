from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        nuevo_nodo = Nodo(valor)
        if not self.cabeza:
            self.cabeza = nuevo_nodo
        else:
            actual = self.cabeza
            while actual.siguiente:
                actual = actual.siguiente
            actual.siguiente = nuevo_nodo

    def buscar_elemento(self, valor):
        actual = self.cabeza
        while actual:
            if actual.valor == valor:
                return True
            actual = actual.siguiente
        return False

    def elimina_cabeza(self):
        if self.cabeza:
            self.cabeza = self.cabeza.siguiente

    def elimina_rabo(self):
        if not self.cabeza:
            return

        if not self.cabeza.siguiente:
            self.cabeza = None
            return

        actual = self.cabeza
        while actual.siguiente.siguiente:
            actual = actual.siguiente
        actual.siguiente = None

    def tamagno(self):
        contador = 0
        actual = self.cabeza
        while actual:
            contador += 1
            actual = actual.siguiente
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
